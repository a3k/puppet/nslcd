require 'spec_helper'

on_supported_os.each do |os, f|
  describe 'nslcd' do
    let(:facts) { {} }

    context "on #{os}" do
      let(:facts) do
        f.merge(super())
      end

      describe 'with defaults for all parameters' do
        it { is_expected.to compile.with_all_deps }

        it { is_expected.to contain_class('nslcd::install') }
        it { is_expected.to contain_class('nslcd::config') }
        it { is_expected.to contain_class('nslcd::service') }

        it { is_expected.to contain_service('nslcd').with_ensure('running') }

        case f[:os]['family']
        when 'Debian'
          it { is_expected.to contain_package('nslcd').with_ensure('present') }

          it do
            is_expected.to contain_file('nslcd.conf') \
              .with_ensure('present') \
              .with_path('/etc/nslcd.conf') \
              .with_content(%r{^# Managed by Puppet\.\n\nuid nslcd\ngid nslcd\nuri ldap:\/\/ldap.example.com\nbase dc=example,dc=com})
          end

          it { is_expected.not_to contain_package('authconfig') }
        when 'RedHat'
          it { is_expected.to contain_package('nss-pam-ldapd').with_ensure('present') }

          it { is_expected.to contain_package('authconfig').with_ensure('present') }
          it { is_expected.to contain_exec('authconfig-mkhomedir').that_comes_before('File[nslcd.conf]') }
        end
      end

      describe 'with service ensure stopped' do
        let(:params) { { service_ensure: 'stopped' } }

        it { is_expected.to contain_service('nslcd').with_ensure('stopped') }
      end

      describe 'with package_ensure set to specific version' do
        let(:params) { { nslcd_package_ensure: '1.1.1' } }

        case f[:os]['family']
        when 'Debian'
          it { is_expected.to contain_package('nslcd').with_ensure('1.1.1') }
        when 'RedHat'
          it { is_expected.to contain_package('nss-pam-ldapd').with_ensure('1.1.1') }
        end
      end

      describe 'with service dependencies' do
        let(:params) { { service_dependencies: ['foo'] } }

        it { is_expected.to contain_service('foo').with_ensure('running') }
      end

      if f[:os]['family'] == 'Debian'
        describe 'with debian mkhomdir enabled' do
          let(:params) { { mkhomedir: true } }

          it do
            is_expected.to contain_file('/usr/share/pam-configs/pam_mkhomedir') \
              .with_ensure('file') \
              .with_content(%r{^# Managed by Puppet\.\nName: activate mkhomedir})
          end
        end
      end
    end
  end
end
