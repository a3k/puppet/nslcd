# To check the correct dependancies are set up for nslcd.

require 'spec_helper'
describe 'nslcd' do
  let(:facts) { { is_virtual: false } }
  let :pre_condition do
    'file { "foo.rb":
      ensure => present,
      path => "/etc/tmp",
      notify => Service["nslcd"] }'
  end

  on_supported_os.each do |os, f|
    context "on #{os}" do
      let(:facts) do
        f.merge(super())
      end

      it { is_expected.to compile.with_all_deps }
      describe 'Testing the dependancies between the classes' do
        it { is_expected.to contain_class('nslcd::install') }
        it { is_expected.to contain_class('nslcd::config') }
        it { is_expected.to contain_class('nslcd::service') }
        it { is_expected.to contain_class('nslcd::install').that_comes_before('Class[nslcd::config]') }
        it { is_expected.to contain_class('nslcd::service').that_subscribes_to('Class[nslcd::config]') }
        it { is_expected.to contain_file('foo.rb').that_notifies('Service[nslcd]') }
      end
    end
  end
end
