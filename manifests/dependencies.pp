# @summary
#   This class handles nslcd service dependencies.
#
# @api private
#
class nslcd::dependencies{

  if ! empty($nslcd::service_dependencies) {
    ensure_resource('service', $nslcd::service_dependencies,
      {
        ensure     => 'running',
        hasstatus  => true,
        hasrestart => true,
        enable     => true,
      }
    )
  }
}
