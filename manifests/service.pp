# @summary
#   This class handles nslcd service.
#
# @api private
#
class nslcd::service {

  ensure_resource('service', $nslcd::nslcd_service,
    {
      ensure     => $nslcd::service_ensure,
      enable     => true,
      hasstatus  => true,
      hasrestart => true,
    }
  )
}
