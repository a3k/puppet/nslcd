# @summary
#   This class handles nslcd config.
#
# @api private
#
class nslcd::config {

  $config_merge = deep_merge($nslcd::base_config, $nslcd::config)

  file { 'nslcd.conf':
    ensure  => $nslcd::ensure,
    path    => $nslcd::config_file,
    owner   => 'root',
    group   => 'root',
    mode    => '0600',
    content => template($nslcd::config_template),
  }

  case $::osfamily {

    'Redhat': {

      $authconfig_flags = $nslcd::mkhomedir ? {
        true  => join($nslcd::enable_mkhomedir_flags, ' '),
        false => join($nslcd::disable_mkhomedir_flags, ' '),
      }
      $authconfig_update_cmd = "/usr/sbin/authconfig ${authconfig_flags} --update"
      $authconfig_test_cmd   = "/usr/sbin/authconfig ${authconfig_flags} --test"
      $authconfig_check_cmd  = "/usr/bin/test \"`${authconfig_test_cmd}`\" = \"`/usr/sbin/authconfig --test`\""

      exec { 'authconfig-mkhomedir':
        command => $authconfig_update_cmd,
        unless  => $authconfig_check_cmd,
      }
      Exec[ 'authconfig-mkhomedir' ] -> File[ 'nslcd.conf' ]
    }

    'Debian': {

      if $nslcd::mkhomedir {

        exec { 'pam-auth-update':
          path        => '/bin:/usr/bin:/sbin:/usr/sbin',
          refreshonly => true,
        }

        file { '/usr/share/pam-configs/pam_mkhomedir':
          ensure  => file,
          owner   => 'root',
          group   => 'root',
          mode    => '0644',
          content => template('nslcd/pam_mkhomedir.erb'),
          notify  => Exec['pam-auth-update'],
        }

      }

    }

    default: { }

  }

}
