# == Class: nslcd 
#
# Installs and configures NSLCD
#
# === Parameters
#
# @param ensure
#   Ensure if the nslcd config file is to be present or absent.
#
# @param config
#   Hash containing entire NSLCD config.
#
# @param nslcd_package
#   Name of the nslcd package. Only set this if your platform is not supported or
#   you know what you're doing.
#
# @param nslcd_package_ensure
#   Sets the ensure parameter of the nslcd package.
#
# @param nslcd_service
#   Name of the nslcd service.
#
# @param extra_packages
#   Array of extra packages.
#
# @param extra_packages_ensure
#   Boolean. Ensure if extra packages are to be installed.
#
# @param config_file
#   Path to the nslcd config file.
#
# @param config_template
#   Defines the template used for the nslcd config.
#
# @param mkhomedir
#   Boolean. Manage auto-creation of home directories on user login.
#
# @param service_ensure
#   Ensure if services should be running/stopped
#
# @param service_dependencies
#   Array of service resource names to manage before managing nslcd related
#   services. Intended to be used to manage messagebus service to prevent
#   `Error: Could not start Service[xxx]`.
#
# @param enable_mkhomedir_flags
#   Flags to use with authconfig to enable auto-creation of home directories.
#
# @param disable_mkhomedir_flags
#   Flags to use with authconfig to disable auto-creation of home directories.
#
# @param debian_mkhomedir_umask
#   Umask to use within debian for auto-creation of home directories.
#
# @param base_config
#   Hash containing OS specific required config parameters.
#
class nslcd (
  Enum['present', 'absent'] $ensure,
  Hash $config,
  String $nslcd_package,
  String $nslcd_package_ensure,
  String $nslcd_service,
  Array $extra_packages,
  String $extra_packages_ensure,
  Stdlib::Absolutepath $config_file,
  String $config_template,
  Boolean $mkhomedir,
  Stdlib::Ensure::Service  $service_ensure,
  Array $service_dependencies,
  Optional[Array] $enable_mkhomedir_flags,
  Optional[Array] $disable_mkhomedir_flags,
  Optional[Integer] $debian_mkhomedir_umask,
  Optional[Hash] $base_config
) {

  contain nslcd::install
  contain nslcd::dependencies
  contain nslcd::config
  contain nslcd::service

  Class['::nslcd::install']
  -> Class['::nslcd::dependencies']
  -> Class['::nslcd::config']
  ~> Class['::nslcd::service']

}
