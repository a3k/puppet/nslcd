# @summary
#   This class handles nslcd packages.
#
# @api private
#
class nslcd::install {

  ensure_resource('package', $nslcd::nslcd_package, { ensure =>  $nslcd::nslcd_package_ensure })

  if $nslcd::extra_packages {
    ensure_packages($nslcd::extra_packages,
      {
        ensure => $nslcd::extra_packages_ensure,
        require => Package[$nslcd::nslcd_package],
      }
    )
  }
}
