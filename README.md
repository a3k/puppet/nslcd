# nslcd 

[![Build Status](https://gitlab.com/a3k/puppet/nslcd/badges/master/pipeline.svg)](https://gitlab.com/a3k/puppet/nslcd)
[![coverage report](https://gitlab.com/a3k/puppet/nslcd/badges/master/coverage.svg)](https://gitlab.com/a3k/puppet/nslcd)
[![Puppet Forge](https://img.shields.io/puppetforge/v/a3k/nslcd.svg)](https://forge.puppetlabs.com/a3k/nslcd)
[![Puppet Forge Downloads](https://img.shields.io/puppetforge/dt/a3k/nslcd.svg)](https://forge.puppetlabs.com/a3k/nslcd)
[![Puppet Forge Score](https://img.shields.io/puppetforge/f/a3k/nslcd.svg)](https://forge.puppetlabs.com/a3k/nslcd/scores)

#### Table of Contents

1. [Overview](#overview)
2. [Usage - Configuration options and additional functionality](#usage)
3. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
4. [Limitations - OS compatibility, etc.](#limitations)
5. [Credits](#credits)

## Overview

This module installs and configures NSLCD (local LDAP name service daemon)

[NSLCD][0] is used to provide access to identity and authentication remote resource through a common framework.

## Usage

Example configuration:

```puppet
class {'::nslcd':
  config => {
    'uid'    => 'nslcd',
    'gid'    => 'ldap',
    'uri'    => 'ldap://ldap.example.com',
    'base'   => 'dc=test,dc=local',
    'scope   => 'one',
  }
}
```

...or the same config in Hiera:

```yaml
nslcd::config:
    'uid': 'nslcd'
    'gid': 'ldap'
    'uri': 'ldap://ldap.example.com'
    'base': 'dc=test,dc=local'
    'scope': 'one'
```

Will be represented in nslcd.conf like this:

```ini
uid nslcd
gid ldap
uri ldap://ldap.example.com
base dc=test,dc=local
scope one
```

## Reference

#####`ensure`
Defines if nslcd and its relevant packages are to be installed or removed. Valid values are 'present' and 'absent'.
Type: string
Default: present

#####`config`
Configuration options structured like the nslcd.conf file. Array values will be joined into comma-separated lists.
Type: hash
Default:
```puppet
config => {
    'uid'    => 'nslcd',
    'gid'    => 'ldap',
    'uri'    => 'ldap://ldap.example.com',
    'base'   => 'dc=test,dc=local',
    'scope   => 'one',
}
```

#####`mkhomedir`
Set to 'true' to enable auto-creation of home directories on user login.
Type: boolean
Default: true

## Limitations

Tested on:
* (CentOS) 7
* Debian 8,9

## Credits

* See CHANGELOG file for additional credits

[0]: https://linux.die.net/man/8/nslcd 
